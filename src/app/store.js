import { configureStore } from '@reduxjs/toolkit';
import { createStore, applyMiddleware } from 'redux';

import reducer, {fetchData} from '../features/ticketsView/ticketFiltersSlice';
import thunk from 'redux-thunk'

const store = createStore(reducer, applyMiddleware(thunk));

export default store;

