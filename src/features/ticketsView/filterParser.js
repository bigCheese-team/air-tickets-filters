export const actualFiltersParser = (data) => {
    const filters = {
        total_duration: [],
        segments_airports: [],
        stops_airports: [],
        transfer_airlines: [],
        transfer_airports: [],
        flights_baggage: [],
        airlines: [],
        arrival_timestamp: [],
        departure_timestamp: [],
        flights_handbags: [],
        max_stop_duration: 0,
        min_stop_duration: 0,
        maxStops: 0,
        maxStopsCount: {}
    }
    data.forEach(dataItems => {
        filters.metadata = {
            airports: dataItems.airports,
            airlines: dataItems.airlines
        }

        let agencyId = dataItems.meta.gates.map(elem => {
            return elem.id
        })

        dataItems.proposals.forEach(proposalsItem => {
            if (filters.maxStops < proposalsItem.max_stops) {
                filters.maxStops = proposalsItem.max_stops
            }
            if (!filters.maxStopsCount.hasOwnProperty(proposalsItem.max_stops)) {
                filters.maxStopsCount[proposalsItem.max_stops] = 0;
            }
            filters.maxStopsCount[proposalsItem.max_stops] += 1;

            filters.total_duration.push(proposalsItem.total_duration)
            filters.max_stop_duration = filters.max_stop_duration < proposalsItem.max_stop_duration
                ? proposalsItem.max_stop_duration
                : filters.max_stop_duration

            filters.min_stop_duration = filters.min_stop_duration >= proposalsItem.min_stop_duration
                ? proposalsItem.min_stop_duration
                : filters.min_stop_duration

            //Baggage
            proposalsItem.terms[agencyId].flights_baggage.forEach((baggageItem, i) => {
                baggageItem.forEach(baggageRule => {
                    let id = ''
                    agencyId.forEach(elem => id = elem)
                    const isInclude = filters.flights_baggage.findIndex(id => id.code) === -1
                    if (baggageRule && isInclude) {
                        filters.flights_baggage.push({
                            id: i,
                            code: baggageRule,
                            terms: String(id)
                        })
                    }
                })
            })

            //Handbag
            proposalsItem.terms[agencyId].flights_handbags.forEach(handbagsItem => {
                handbagsItem.forEach(item => {
                    if (item && filters.flights_handbags.indexOf(item) === -1) {
                        filters.flights_handbags.push(item)
                    }
                })
            })

            //Airlines
            proposalsItem.carriers.forEach(carriesItem => {
                if (filters.airlines.indexOf(carriesItem) === -1) {
                    filters.airlines.push(carriesItem)
                }
            })

            //Airports
            let airportsStr = ""
            proposalsItem.segments_airports.forEach(airportsList => {

                airportsList.forEach(airports => {
                    airportsStr += airports
                })

                if (filters.segments_airports.indexOf(airportsStr) === -1) {
                    filters.segments_airports.push(airportsStr)
                }
                airportsStr = ""
            })
            // Stops airports
            filters.stops_airports.push(proposalsItem.stops_airports)


            proposalsItem.segment.forEach(segmentItem => {

                if (segmentItem.transfers) {
                    segmentItem.transfers.forEach(transferItem => {
                        //Transfers airports
                        transferItem.airports.forEach(airportsItem => {
                            if (filters.transfer_airports.indexOf(airportsItem) === -1) {
                                filters.transfer_airports.push(airportsItem)
                            }
                        })
                        //Transfers airlines
                        transferItem.airlines.forEach(airlinesItem => {
                            if (filters.transfer_airlines.indexOf(airlinesItem) === -1) {
                                filters.transfer_airlines.push(airlinesItem)
                            }
                        })
                    })
                }
                //Arrival departure time
                segmentItem.flight.forEach(flightItem => {
                    filters.arrival_timestamp.push(flightItem.arrival_timestamp)
                    filters.departure_timestamp.push(flightItem.departure_timestamp)
                })
            })

        })

    })

    console.log("FILTERS Normalised", _normaliseFilterObj(filters))
    console.log("FILTERS", filters)
    return _normaliseFilterObj(filters)
}

export const getProposals = (data) => {
    let proposalsList = []
    data.forEach(elem => {
        elem.proposals.forEach(proposals => {
            proposalsList.push(proposals)
        })
    })
    return proposalsList
}


export const getMetadata = (data) => {
        let airports = []
        let airlines = []

        data.forEach(elem => {
            airports = elem.airports
            airlines = elem.airlines
        })
        return {airports, airlines}
    }


const _normaliseFilterObj = (data) => {

    const {metadata, maxStopsCount} = data

    let total_duration,
        segments_airports = [],
        stops_airports = [],
        transfer_airlines = [],
        transfer_airports = [],
        flights_baggage = [],
        airlines = [],
        arrival_timestamp = [],
        departure_timestamp = [],
        flights_handbags = []
    let max_stop_duration,
        min_stop_duration,
        maxStops = [];

    const transfersMeta = [
        "Без пересадок",
        "Одна пересадка",
        "Две пересадки",
        "Три и более пересадок"
    ];
    transfersMeta.forEach((elem, i) => {
        if (i <= data.maxStops) {
            maxStops.push({
                id: i,
                name: `${elem} (${data.maxStopsCount[i]})`,
                selected: i === 0
            })
        }
    })

    min_stop_duration = data.min_stop_duration

    arrival_timestamp = _createTimePeriodObject(data.arrival_timestamp)
    departure_timestamp = _createTimePeriodObject(data.departure_timestamp)
    stops_airports = data.stops_airports

    max_stop_duration = {
        max: data.max_stop_duration,
        selected: 0
    }

    airlines = data.airlines.map((elem, i) => {
        return {
            id: i,
            name: i === 0 ? "Все" : metadata.airlines[elem].name,
            code: i === 0 ? null : elem,
            selected: i === 0 ? true: false
        }
    })

    segments_airports = data.segments_airports.map((elem, i) => {
        return {
            id: i,
            name: `${metadata.airports[elem.slice(0, 3)].name}/${metadata.airports[elem.slice(3, elem.length)].name}}`,
            code: `${elem.slice(0, 3)}/${elem.slice(3, elem.length)}`,
            selected: i === 0? true: false
        }
    })

    transfer_airlines = data.transfer_airlines.map((elem, i) => {
        return {
            name: metadata.airlines[elem].name,
            code: elem,
            selected: i === 0 ? true : false
        }
    })

    transfer_airports = data.transfer_airports.map((elem, i) => {
        return {
            id: i,
            name: metadata.airports[elem].name,
            code: elem,
            selected: i === 0 ? true : false
        }
    })


    flights_baggage = (() => {
        let baggage = []
        baggage.push({id: 0, name: "Все", selected: true})
        if (data.flights_baggage.length > 0 ) {
            baggage.push({id: 1, name: "Только багаж", selected: false})
        }
        if(data.flights_handbags.length > 0) {
            baggage.push({id: 2, name: "Только ручная кладь", selected: false})
        }
        return baggage
    })()


    total_duration = {
        maxTotalDuration: 0,
        minTotalDuration: 0,
        selected: 0
    }
    data.total_duration.forEach(item => {
        if(total_duration.maxTotalDuration < item * 1) {
            total_duration.maxTotalDuration = item
        }
        if(total_duration.minTotalDuration < item) {
            total_duration.minTotalDuration = item
        }
    })

    return  {
        total_duration,
        segments_airports,
        stops_airports,
        transfer_airlines,
        transfer_airports,
        flights_baggage,
        airlines,
        arrival_timestamp,
        departure_timestamp,
        flights_handbags,
        max_stop_duration,
        min_stop_duration,
        maxStops,
        maxStopsCount
    }
}


const _createTimePeriodObject = (timeStampList) => {
    const periodBoundaries = _getTimePeriodBoundaries(timeStampList)
    const {minDate, maxDate} = periodBoundaries
    const periodDuration = _getPeriodDuration({minDate, maxDate})
    return  {
        period_duration: periodDuration,
        min_date: {
            date: minDate,
            time: _getTimeInFormat(minDate)
        },
        max_date: {
            date: maxDate,
            time: _getTimeInFormat(maxDate)
        }
    }
}

const _getTimePeriodBoundaries = (timeStampList) => {
    let maxDate = 0
    let minDate = timeStampList[0] * 1000
    timeStampList.forEach(itemTime => {
        let date = new Date(itemTime * 1000)
        if(maxDate < date) {
            maxDate = date
        }
        if(minDate > date) {
            minDate = date
        }
    })

    return {minDate, maxDate}
}

const _getPeriodDuration = ({minDate, maxDate}) => {
    return Math.ceil(Math.abs(maxDate.getTime() - minDate.getTime()) / (1000 * 3600 ))
}

const _getTimeInFormat = (date) => {
    const hours = new Date(date).getHours()
    const minutes  = new Date(date).getMinutes()
    return `${hours}:${minutes}`
}

