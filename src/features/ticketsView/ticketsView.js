import React, {Component} from 'react';
import {connect} from 'react-redux';

import {Container, Row, Col, Spinner} from 'reactstrap';

import FilterPanel from '../../components/filterPanel/filterPanel';
import TicketList from '../../components/ticketsList/ticketsList';
import {fetchData} from './ticketFiltersSlice';

class TicketsView extends Component {

    componentDidMount() {
        const {fetchTicketsData} = this.props
        fetchTicketsData()
    }

    render() {
        const {loading} = this.props.store
        return (
            <Container fluid>
                <Row>
                    <Col md="2">
                        List of filters
                        {loading === 'idle'
                            ? <FilterPanel />
                            : <Spinner color="info" className="m-5 p-5" />}
                    </Col>
                    <Col md="10">
                        {loading === 'idle'
                            ? <TicketList />
                            : <Spinner color="info" className="m-5 p-5" />}

                    </Col>
                </Row>
            </Container>
        )
    }
}

const mapStateToProps = (store) => {
    return {
        store: store
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchTicketsData: () => dispatch(fetchData())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TicketsView)


