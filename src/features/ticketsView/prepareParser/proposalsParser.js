export const proposalsParser = (data) => {
    const proposalsList = []

    data.forEach(dataItems => {
        //Metadata
        let agencyId = dataItems.meta.gates.map(elem => {
            console.log(elem.id)
            return elem.id
        })

        console.log('agencyId', agencyId)

        //Get Item Object
        dataItems.proposals.forEach((proposalsItem, i) => {
            let proposalsResult = {
                airlines: [],
                flights_baggage: []
            }

            //Max stops
            proposalsResult.maxStops = proposalsItem.max_stops

            //Airlines
            proposalsResult.airlines = proposalsItem.carriers.map(airline => {
                return {
                    code: airline,
                    name: dataItems.airlines[airline].name
                }
            })

            // Baggage
            let id = ''
            agencyId.forEach(elem => id = elem)
            let isBaggage = false
            let isHandbag = false
            proposalsItem.terms[id].flights_baggage.forEach((baggageItem, i) => {
                baggageItem.forEach(baggageRule => {
                    if (baggageRule !== false) {
                        isBaggage = true
                    }
                })
            })
            proposalsItem.terms[id].flights_handbags.forEach(handbagItem => {
                handbagItem.forEach(handbagsRule => {
                    if (handbagsRule !== false) {
                        isHandbag = true
                    }
                })
            })

            // Collect Ticket object
            proposalsItem.segment.forEach(segmentItem => {
                let ratingTotal = segmentItem.rating.total
                let ratingArrivalTime = segmentItem.rating.detailed.arrival_time

                segmentItem.flight.forEach(flightItem => {
                    proposalsList.push({
                        aircraft: flightItem.aircraft,
                        arrival: flightItem.arrival,
                        airlines: {
                            name: dataItems.airlines[flightItem.operating_carrier].name,
                            code: flightItem.operating_carrier
                        },
                        airports: {
                            airportArrival: {
                                name: dataItems.airports[flightItem.arrival].name,
                                code: flightItem.arrival
                            },
                            airportDeparture: {
                                name: dataItems.airports[flightItem.departure].name,
                                code: flightItem.departure
                            },
                        },
                        stops: proposalsItem.max_stops,
                        technical_stops: {
                            count: flightItem.technical_stops? flightItem.technical_stops.length: 0,
                            airport_code: flightItem.technical_stops
                                ? flightItem.technical_stops.map(itemStops => itemStops.airport_code)
                                : null
                        },
                        arrival_timestamp: flightItem.arrival_timestamp,
                        departure_timestamp: flightItem.departure_timestamp,
                        duration: flightItem.duration,
                        flights_baggage: {
                            baggage: isBaggage,
                            handbag: isHandbag
                        },
                        rating_total: ratingTotal,
                        arrival_time: ratingArrivalTime
                    })
                })

            })
        })
    })
    console.log('Parser tickets', proposalsList)
    return proposalsList
}