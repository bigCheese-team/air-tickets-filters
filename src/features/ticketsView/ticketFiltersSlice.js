import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';

import {getResource} from '../../service/service';
import {actualFiltersParser, getMetadata, getProposals} from './filterParser'
import {proposalsParser} from './prepareParser/proposalsParser'


const initialState = {
    data: [],
    tickets: [],
    proposals: [],
    actualFilters: {
        maxStops: [],
        airlines: [],
        flights_baggage: [],
        transfer_airports:[],
        arrival_timestamp:{
           
        }
    },
    ticketList: [],
    metadata: {
        airports:{},
        airlines: {}
        },
    loading: 'idle'
};

export const fetchData = createAsyncThunk(
    'ticketList/fetchAllTickets',
    async () => {
        const response = await getResource()
        return response
    }
)

export const ticketFilterSlice = createSlice({
    name: "ticketList",
    initialState,
    reducers: {
        doSomething: (state, action) => {
           console.log(`Action run with: ${action.payload}`)
        },

        sortByArrivalTime: (state, action) => {
            state.actualFilters.arrival_timestamp.date_for_filter = action.payload
        },

        sortByDuration: (state, action) => {
            /**TODO Action for sort by duration */
        },

        sortByTransferCount: (state, action) => {
            state.actualFilters.maxStops.forEach(elem => {
                if(elem.id === action.payload) {
                    elem.selected = true
                } else {
                    elem.selected = false
                }
             })
        },
        sortByAirlines: (state, action) => {
            state.actualFilters.airlines.forEach(elem => {
                if(elem.id === action.payload) {
                    elem.selected = true
                } else {
                    elem.selected = false
                }
            })
    },
        sortByBaggage: (state, action) => {
            state.actualFilters.flights_baggage.forEach(elem => {
                if(elem.id === action.payload) {
                    elem.selected = true
                } else {
                    elem.selected = false
                }
            })
        },
        sortByAirports: (state, action) => {
            state.actualFilters.transfer_airports.forEach(elem => {
                if(elem.id === action.payload) {
                    elem.selected = true
                } else {
                    elem.selected = false
                }
            })
        },
        setFiltersState:(state, action) => {
            state.actualFilters.stops_count = action.payload
        },
    },
    extraReducers: {
        [fetchData.pending]: (state) => {
            if(state.loading === 'idle') {
                state.loading = 'pending'
            }
        },
        [fetchData.fulfilled]: (state, action) => {
            if(state.loading === 'pending') {
                state.data = action.payload
                state.proposals = getProposals(action.payload)
                state.actualFilters = actualFiltersParser(action.payload)
                state.metadata = getMetadata(action.payload)
                state.ticketList = proposalsParser(action.payload)
                state.loading = 'idle'
            }
        },

        [fetchData.rejected]: (state, action) => {
            console.log(action.error)
        }
    }
})


export const { doSomething,

    sortByTransferCount,
    sortByAirlines,
    sortByBaggage,
    sortByAirports,
    sortByArrivalTime
    } = ticketFilterSlice.actions

export default ticketFilterSlice.reducer

