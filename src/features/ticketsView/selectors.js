import { createSelector } from "reselect";


export const getActualFilters = store => store.actualFilters
export const actualFilters = store => store.actualFilters
export const getProposals = store => store.proposals
export const getTicketListSelector = store => store.ticketList


const filterByTransfersCountSelector = createSelector(
    [getTicketListSelector, getActualFilters],
    (proposals, actualFilters) => {
        const {maxStops} = actualFilters
        let filter = ''
        let filteredProposals = []
        maxStops.forEach(elem => {
            if (elem.selected === true) {
                filter = elem.id
            }
        });
        
        filteredProposals = proposals.filter(elem => {
            return elem.stops === filter
        });

        return filteredProposals;
    }
)

const filterByAirlinesSelector = createSelector(
    [filterByTransfersCountSelector, getActualFilters],
    (proposals, actualFilters)  => {
        const {airlines} = actualFilters
        let filterCode = ''
        let filteredProposals = []
        airlines.forEach(elem => {
            if (elem.selected === true) {
                filterCode = elem.code
            }
        })
        filteredProposals = proposals.filter(elem => {
            if (!filterCode) {
                return true
            } else {
                return elem.airlines.code === filterCode
            }
        })

        return filteredProposals
    }
)

const filterByBaggage = createSelector(
    [filterByAirlinesSelector, getActualFilters],
    (proposals, actualFilters) => {
        const {flights_baggage} = actualFilters
        let filterId = 0
        let filteredProposals = []
        flights_baggage.forEach(elem => {
            if(elem.selected === true) {
                filterId = elem.id
            }
        })
        filteredProposals = []
        switch (filterId) {
            case 0: {
                filteredProposals = proposals
                break
            }
            case 1: {
                filteredProposals = proposals.filter(proposalsElem => proposalsElem.flights_baggage.baggage)
                break
            }
            case 2: {
                filteredProposals = proposals.filter(proposalsElem => proposalsElem.flights_baggage.handbag)
                break
            }
            default: filteredProposals = proposals
        }
        return filteredProposals
    }
)

const filterByTransferAirports = createSelector(
    [getProposals, getActualFilters],
    (proposals, actualFilters) => {
        const {transfer_airports} = actualFilters
        let filter = ''
        let newProposals = []
        transfer_airports.forEach(elem => {
            if(elem.selected) {
                filter = elem.code
            }
        })
        newProposals = proposals.filter(proposal => {
            let isCorrect = false
            proposal.segment.forEach(segmentItem => {
                if (segmentItem.transfers) {
                    segmentItem.transfers.forEach(transfer => {
                        transfer.airports.forEach(airport => {
                            filter === airport
                                ? isCorrect = true : isCorrect = false
                        })
                    })
                }
            })
            return isCorrect
        })
        return newProposals
    }
)

const filterByArrivalTimes = createSelector(
    [filterByBaggage, getActualFilters],
    (proposals, actualFilters) => {
        let newProposals = proposals
        const {date_for_filter} = actualFilters.arrival_timestamp
        if(date_for_filter) {
            newProposals = proposals.filter(proposal => {
                const arrivalDate = new Date(proposal.arrival_timestamp * 1000)
                return arrivalDate < date_for_filter
            })
        }
    return newProposals
    }
)



export const filterSelector = createSelector(
    filterByArrivalTimes,
    (_filterByTransferAirports) => {
        return _filterByTransferAirports
    }
)