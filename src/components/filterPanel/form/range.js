import React, {useState} from 'react';
import { FormGroup, Label, Input} from 'reactstrap';
import { useDispatch } from 'react-redux';


const Range = ({title, action, min = 0, max = 10, date = ''}) => {
    const [value, setValue] = useState(0);
    const dispatch = useDispatch();


    const onChangeValue = (eventValue) => {
        const dataValue = new Date(date)
        if (eventValue > value) {
            dataValue.setHours(dataValue.getHours() + parseInt(eventValue))
        } else {
            dataValue.setHours(dataValue.getHours() - parseInt(eventValue))
        }
        action.payload = dataValue
        dispatch(action)
        setValue(eventValue)
    }

    return (
        <div>
            <label for="volume">{title}</label>
            <input onChange={(e) => onChangeValue(e.target.value)} value={value} type="range" id="volume" name="volume"
                   min={min} max={max} />
        </div>

    );
}

export default Range;