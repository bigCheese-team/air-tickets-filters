import React, {useState} from 'react'
import {useDispatch} from "react-redux";

const Checkbox = ({title, action, selected, id}) => {
    let [isCheck, onCheck] = useState(false)
    const dispatch = useDispatch();
    return (
        <div>
            <input type="checkbox" onChange={() => {
                onCheck(isCheck = !isCheck)
                dispatch(action(id))
            }} checked={selected} defaultValue={false}/>
            <span className="ml-2">{title}</span>
        </div>
    )
}

export default Checkbox