import React from "react";
import Airlines from "./filters/airlines";
import AirPorts from "./filters/airports";
import Baggage from "./filters/baggage";
import DepartureArrivalTime from "./filters/departureArrivalTimes";
import Transfers from "./filters/transfers";
import TransferAirports from "./filters/transferAirports";
import TransferTime from "./filters/transferTime";
import TravelTime from "./filters/travelTime";
import {useSelector} from "react-redux";
import {actualFilters} from "../../features/ticketsView/selectors";



const FilterPanel = () => {
    const filters = useSelector(actualFilters)

    const { maxStops,
            flights_baggage,
            flights_handbags,
            airlines,
            segments_airports,
            total_duration,
            max_stop_duration,
            min_stop_duration,
            arrival_timestamp,
            departure_timestamp,
            transfer_airlines,
            transfer_airports} = filters

    return (
        <div>
                <div>
                    {maxStops && maxStops.length > 1
                        ? <Transfers maxStops={maxStops}/>
                        : null}

                    {airlines
                        ? <Airlines airlines={airlines}/>
                        : null}

                    {flights_handbags
                        ? <Baggage flights_handbags={flights_handbags} flights_baggage={flights_baggage}/>
                        : null}

                    {arrival_timestamp && departure_timestamp
                        ? <DepartureArrivalTime departure_timestamp={departure_timestamp}
                                           arrival_timestamp={arrival_timestamp}/>
                                           : null}

                    {total_duration
                        ? <TravelTime total_duration={total_duration}/>
                        : null}

                    {transfer_airlines
                        ? <TransferAirports transfer_airports={transfer_airports}/>
                        : null}

                    {max_stop_duration
                        ? <TransferTime min_stop_duration={min_stop_duration} max_stop_duration={max_stop_duration}/>
                        : null}

                    {segments_airports
                        ? <AirPorts segments_airports={segments_airports}/>
                        : null}
                </div>

        </div>
    )
}

export default FilterPanel