import React from 'react';

import {sortByBaggage} from '../../../features/ticketsView/ticketFiltersSlice'
import Checkbox from '../form/checkbox';

const Baggage = ({flights_handbags, flights_baggage}) => {

    return (
        <div className="border border-info d-flex flex-column align-items-start">
            <div>
                <p>Багаж</p>
            </div>
            {flights_baggage.map((elem, i) => {
                const {id, name, selected} = elem
                return (
                    <Checkbox key={i}
                              id={id}
                              selected={selected}
                              title={name}
                              action={sortByBaggage}
                    />
                )
            })}
        </div>
    )
}

export default Baggage
