import React from 'react';

import Checkbox from '../form/checkbox';
import {sortByAirports} from '../../../features/ticketsView/ticketFiltersSlice'


const TransferAirports = ({transfer_airports}) => {

    return (
        <div className="border border-info d-flex flex-column align-items-start">
            <div>
                <p>Аэропорты пересадок</p>
            </div>
            {transfer_airports.map((airports, i) => {
                const {name, id, selected} = airports
                return (
                    <Checkbox key={i}
                              title={name}
                              action={sortByAirports}
                              id={id}
                              selected={selected}
                    />
                )

            })}
        </div>
    )
}

export default TransferAirports
