import React from 'react';

import Checkbox from '../form/checkbox';
import {doSomething} from '../../../features/ticketsView/ticketFiltersSlice';


const AirPorts = ({segments_airports, airportMeta}) => {

    return (
        <div className="border border-info d-flex flex-column align-items-start">
            <div>
                <p>Аэропорты</p>
            </div>

            {segments_airports.map((ports, i) => {
                const {name} = ports
                return (
                 <div key={i} className="border mt-2 d-flex flex-column align-items-start border-info">
                     <Checkbox action={doSomething()}
                               title={name}/>
                 </div>
             )
            })}
        </div>
    )
}

export default AirPorts
