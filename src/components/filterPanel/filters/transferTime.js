import React from 'react';

import Range from '../form/range';
import {doSomething} from '../../../features/ticketsView/ticketFiltersSlice';


const TransferTime = ({max_stop_duration}) => {
    const {max} = max_stop_duration
    return (
        <div className="border border-info d-flex flex-column align-items-start">
            <div>
                <p>Время пересадки</p>
            </div>
            <h6>Туда</h6>
            <Range action={doSomething()} max={max} />

        </div>
    )
}

export default TransferTime
