import React from 'react';

import Range from '../form/range';
import {sortByArrivalTime} from '../../../features/ticketsView/ticketFiltersSlice';


const DepartureArrivalTime = ({arrival_timestamp, departure_timestamp}) => {

    return (
        <div className="border border-info d-flex flex-column align-items-start">
            <div>
                <p>Время отправления и прибытия</p>
            </div>
            <h6>Туда</h6>
            <a href="#" className="">Отправление</a>
            <div className="d-flex justify-content-between w-100">
                <span>{arrival_timestamp.min_date.time}</span>
                <span>{arrival_timestamp.max_date.time}</span>
            </div>
            <Range action={sortByArrivalTime()} max={arrival_timestamp.period_duration}
                                            date={arrival_timestamp.min_date.date}/>
            <a href="#" className="">Прибытие</a>
            <div className="d-flex justify-content-between w-100">
                <span>{departure_timestamp.min_date.time}</span>
                <span>{departure_timestamp.max_date.time}</span>
            </div>
            <Range action={sortByArrivalTime()} max={departure_timestamp.period_duration}
                                            date={departure_timestamp.min_date.date}/>
        </div>
    )
}

export default DepartureArrivalTime
