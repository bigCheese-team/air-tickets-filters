import React from 'react';

import Range from '../form/range';
import {doSomething} from '../../../features/ticketsView/ticketFiltersSlice';


const TravelTime = ({total_duration}) => {
    const { maxTotalDuration, minTotalDuration} = total_duration

    return (
        <div className="border border-info d-flex flex-column align-items-start">
            <div>
                <p>Время в пути</p>
            </div>
            <h6>Туда</h6>
            <Range action={doSomething()} max={maxTotalDuration / 60}/>
        </div>
    )
}

export default TravelTime
