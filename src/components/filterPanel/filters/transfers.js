import React from 'react';

import Checkbox from '../form/checkbox';
import {sortByTransferCount} from '../../../features/ticketsView/ticketFiltersSlice';


const Transfers = ({maxStops}) => {
    return (
        <div className="border border-info d-flex flex-column align-items-start">
            <div>
                <p>Пересадки</p>
            </div>
            {maxStops.map(({id, name, selected}, i) => {
                    return (
                        <Checkbox key={i} action={sortByTransferCount}
                                  title={name}
                                  selected={selected}
                                  id={id}/>
                    )
            })}
        </div>
    )
}

export default Transfers
