import React from 'react';

import Checkbox from '../form/checkbox';
import {sortByAirlines} from '../../../features/ticketsView/ticketFiltersSlice';


const Airlines = ({airlines}) => {

    return (
        <div className="border border-info d-flex flex-column align-items-start">
            <div>
                <p>Авиакомпании</p>
            </div>

            {airlines.map(({id, name, selected}, i) => {
                return (
                    <Checkbox key={i} action={sortByAirlines}
                              selected={selected}
                              id={id}
                              title={name}/>
                )
            })}
        </div>
    )
}

export default Airlines
