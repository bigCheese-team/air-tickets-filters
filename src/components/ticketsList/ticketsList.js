import  React from 'react'
// import {useSelector} from "react-redux";
import {useSelector} from 'react-redux';

import {Row, Col} from 'reactstrap';

import {filterByTransfersCountSelector, filterSelector} from '../../features/ticketsView/selectors';


const TicketList = () => {
    const ticketList = useSelector(filterSelector)

    //console.log("TicketList Selector", ticketList)
    return (
        <>
            {
                ticketList.map((proposals, i) => {
                    return (
                        <Row>
                            <Col md={12} className="mt-2 mb-2">
                                <div className="d-flex justify-content-around">
                                   <div>
                                       <span>Aircraft</span>
                                       <p>{proposals.aircraft}</p>
                                   </div>
                                    <div>
                                        <span>Airlines</span>
                                        <p>{proposals.airlines.name}</p>
                                    </div>
                                    <div>
                                        <span>Airports</span>
                                        <p><span>Отправление: </span>{proposals.airports.airportDeparture.name}</p>
                                        <p><span>Прибытие: </span>{proposals.airports.airportArrival.name}</p>
                                    </div>
                                    <div>
                                        <span>Arr/Dep time</span>
                                        <p>{proposals.arrival_timestamp}</p>
                                        <p>{proposals.departure_timestamp}</p>
                                    </div>
                                    <div>
                                        <span>Duration</span>
                                        <p>{proposals.duration}</p>
                                    </div>
                                    <div>
                                        <span>Flight baggage</span>
                                        <p>{proposals.flights_baggage.baggage? "С багажем":"Без багажа"}</p>
                                        <p>{proposals.flights_baggage.handbag? "Ручная кладь": "Без Ручной клади"}</p>
                                    </div>
                                    <div>
                                        <span>Technical Stops</span>
                                        <p>{proposals.technical_stops.count}</p>
                                    </div>
                                </div>
                                <hr/>
                            </Col>
                        </Row>
                    )
                })
            }

        </>
    )
}

export default TicketList




// return (
//     <>
//         {
//             proposalsSelector.map((proposals, i) => {
//                 return (
//                     <div className="w-100" key={`prop${i}`}>
//                         {proposals.segment.map(segmentItem => {
//                             return (
//                                 <>
//                                     { segmentItem.flight.map(flightItem => {
//                                         return (
//                                             <p>{flightItem.aircraft}</p>
//                                         )
//                                     })
//                                     }
//                                 </>
//                             )
//                         })}
//                         {proposals.segments_airports.map((airports, i) => {
//                             return (
//                                 <p key={`airports${i}`}>{airports.map((airport, i) => {
//                                     return (
//                                         <span key={`air${i}`}>
//                                            {metadata.airports[airport].city} <br />
//                                        </span>
//                                     )
//                                 })}</p>
//                             )
//                         })}
//                     </div>
//                 )
//             })
//         }
//
//     </>
// )
