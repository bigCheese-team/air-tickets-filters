import React from 'react';
import './App.css';
import TicketsView from "./features/ticketsView/ticketsView";

function App() {
  return (
    <div className="App">
      <TicketsView/>
    </div>
  );
}

export default App;
